# metaweather-unit

A unit test project for https://www.metaweather.com/api/.

# Tested Operating System
- Kubuntu 20.04

# Requirements
- openjdk 14
- Gradle 6

# Quick Start
## Installation
1. Install openjdk 14
```script
sudo apt install openjdk-14-jre
```
2. Install Gradle 6
```script
sudo add-apt-repository ppa:cwchien/gradle
sudo apt-get update
sudo apt install gradle
```

## How to Build and run
```script
./gradlew build -i
```

## Viewing test report
With any prefered browser, open the unit test report html page:
```
<Project_DIR>/build/reports/tests/test/index.html
```