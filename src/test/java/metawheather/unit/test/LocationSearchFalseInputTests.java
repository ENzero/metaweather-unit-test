/**
 * @file LocationSearchFalseInputTests.java
 * @author Angelo Sison<angelo.gitb@gmail.com>
 * @date 26/7/2020
 * @brief File contains unit test to test odd cases for location search
 *
 */
package metaweather.unit.test;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class LocationSearchFalseInputTests {

    private String locationInput;
    private int expectedWoeId;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {

        return Arrays.asList(new Object[][] {
            { "Nottingham", 30720}, 
            { "noTTingham", 30720}, 
            { "NOTTINGHAM", 30720}, 
            { "30720", 0}, 
            { "notti_ngham", 0}, 
            { "notti_ngham", 0}, 
            { "0", 0}, 
            { "-1", 0}, 
            { "?", 0}, 
            { "Europe", 0} });
    }

    public LocationSearchFalseInputTests(String input, int expected)
    {
        this.locationInput = input;
        this.expectedWoeId = expected;
    }

    @Before
    public void setUp() {
        System.out.println("\n-----------------------------------------------------------");
    }
    
    /**
     * @brief Test Location query
     * 
     * - Input condition: Odd location string
     * - Expected output: Json data containing the correct WOE ID value.
     * 
     * If expectedWoeId is zero, the testcase is expected to fail; Otherwise,
     * it should succeed and match the WOE ID of the location input.
     */
    @Test
    public void Location_InputCorrectLocation_woeIdIsCorrect() 
    {
        // ARRANGE
        String urlInput = MetaWeatherApiInterface.ROOT_ENDPOINT + "/api/location/search/?query=" + locationInput;
        System.out.println("location input: " + urlInput);
        System.out.println("expected WOE ID: " + expectedWoeId);
        
        // ACT
        String outputJson = MetaWeatherApiInterface.getRequest(urlInput);
        System.out.println("Get request output: " + outputJson);
        
        // ASSERT
        MetaWeatherApiInterface.LocationData locationOutput = MetaWeatherApiInterface.parseLocationMessage(outputJson);
        System.out.println("LocationData : " + locationOutput);
        
        if(expectedWoeId == 0)
        {
            assertNull("The location query \"" + locationInput + "\" should be invalid", locationOutput);
        }
        else
        {
            assertEquals("Woed \"" + locationOutput.woeId + "\"Did not match " + locationInput,
                locationOutput.woeId, expectedWoeId);
        }
    }
}