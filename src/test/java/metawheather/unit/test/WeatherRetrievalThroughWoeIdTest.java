/**
 * @file WeatherRetrievalThroughWoeIdTest.java
 * @author Angelo Sison<angelo.gitb@gmail.com>
 * @date 26/7/2020
 * @brief Test wheather query thorugh WOE ID.
 *
 */

package metaweather.unit.test;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import org.joda.time.*;

@RunWith(Parameterized.class)
public class WeatherRetrievalThroughWoeIdTest 
{
    private int woeIdInput;
    private String  expectedTitle;
    private int currentDay;
    private int currentMonth;
    private int currentYear;


    @Parameterized.Parameters
    public static Collection<Object[]> data() 
    {
        return Arrays.asList(new Object[][] {
            { 30720, "Nottingham"}, 
            { 615702, "Paris"} , 
            { 676757, "Munich"} });
    }

    public WeatherRetrievalThroughWoeIdTest(int input, String expected)
    {
        this.woeIdInput = input;
        this.expectedTitle = expected;

        /* Set Current date */
        DateTime testSetUp = new DateTime();
        LocalDate today = testSetUp.toLocalDate();
        this.currentDay = today.getDayOfMonth();
        this.currentMonth = today.getMonthOfYear();
        this.currentYear = today.year().get();
    }

    @Before
    public void setUp() {
       System.out.println("\n-----------------------------------------------------------");
    }

    /**
     * @brief Test that the correct location is returned
     * - Input condition: WOEID value
     * - Expected output: The correct location name
     */
    @Test
    public void Weather_InputCorrectLocation_TitleIsCorrect() 
    {
        // ARRANGE
        String urlInput = MetaWeatherApiInterface.ROOT_ENDPOINT + "/api/location/" + woeIdInput +"/";
        System.out.println("WOE ID input: " + urlInput + "\n");
        System.out.println("expected title: " + expectedTitle);
        
        // ACT
        String outputJson = MetaWeatherApiInterface.getRequest(urlInput);
        System.out.println("Get request output: " + outputJson);
        
        // // ASSERT
        MetaWeatherApiInterface.WeatherData weatherData = MetaWeatherApiInterface.parseWeatherMessage(outputJson);
        assertEquals("Title did not match", weatherData.title, expectedTitle);
    }

    /**
     * @brief Test that the correct location is returned
     * - Input condition: WOEID value
     * - Expected output: today's date
     */
    @Test
    public void Weather_InputCorrectLocation_ReturnsCurrentDate() 
    {
        // ARRANGE
        String urlInput = MetaWeatherApiInterface.ROOT_ENDPOINT + "/api/location/" + woeIdInput +"/";
        System.out.println("WOE ID input: " + urlInput + "\n");

    
        DateTime testSetUp = new DateTime();
        LocalDate today = testSetUp.toLocalDate();
        System.out.println("Expected day: " + today.getDayOfMonth());
        System.out.println("Expected month: " + today.getMonthOfYear());
        System.out.println("Expected year: " + today.year().get());

        // ACT
        String outputJson = MetaWeatherApiInterface.getRequest(urlInput);
        System.out.println("Get request output: " + outputJson);
        
        // ASSERT
        MetaWeatherApiInterface.WeatherData weatherData = MetaWeatherApiInterface.parseWeatherMessage(outputJson);
        assertEquals("Did not match current day", weatherData.time.getDayOfMonth(), this.currentDay);
        assertEquals("Did not match current month", weatherData.time.getMonthOfYear(), this.currentMonth);
        assertEquals("Did not match current year", weatherData.time.year().get(), this.currentYear);
    }
}