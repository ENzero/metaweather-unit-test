/**
 * @file WeatherRetrievalThroughDateTest.java
 * @author Angelo Sison<angelo.gitb@gmail.com>
 * @date 26/7/2020
 * @brief Test wheather date accuracy
 *
 */

package metaweather.unit.test;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.joda.time.*;

@RunWith(Parameterized.class)
public class WeatherRetrievalThroughDateTest {

    private static final int nottinghamWoeId = 30720;
    private static final int dateTestRange = 10;
    private static final int dateTestArraySize = dateTestRange * 2;
    private static final int dateValues = 3;

    private int dayInput;
    private int monthInput;
    private int yearInput;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {

        DateTime testSetUp = new DateTime();
        LocalDate today = testSetUp.toLocalDate();
        System.out.println("Current day: " + today.getDayOfMonth());
        System.out.println("Current month: " + today.getMonthOfYear());
        System.out.println("Current year: " + today.year().get());

        Object[][] dateTestInput  = new Object[dateTestArraySize][dateValues];

        int index = 0;
        int dateCounter = 0;
        for(index = 0, dateCounter = (dateTestRange*(-1)); 
            index < dateTestArraySize; 
            index++, dateCounter++)
        {
            LocalDate tempDate = today.plusDays(dateCounter);
            dateTestInput[index][0] = tempDate.getDayOfMonth();
            dateTestInput[index][1] = tempDate.getMonthOfYear();
            dateTestInput[index][2] = tempDate.year().get();
        }

        return Arrays.asList(dateTestInput);
    }

    public WeatherRetrievalThroughDateTest(int day, int month, int year)
    {
        this.dayInput = day;
        this.monthInput = month;
        this.yearInput = year;
    }

    @Before
    public void setUp() {
       System.out.println("\n-----------------------------------------------------------");
    }
    
    /**
     * @brief Test correct wheather forcast dates
     * This is tested on Nottingham.
     * 
     * - Input condition: day, month, and year integer values
     * - Expected output: All wheather forcasts should return the correct date
     */
    @Test
    public void Weather_InputCorrectLocation_titleIsCorrect() 
    {
        // ARRANGE
        String urlInput = MetaWeatherApiInterface.ROOT_ENDPOINT 
            + "/api/location" 
            + "/" + nottinghamWoeId 
            + "/" + yearInput
            + "/" + monthInput
            + "/" + dayInput
            + "/";

        System.out.println("URL input: " + urlInput);
        
        // ACT
        String outputJson = MetaWeatherApiInterface.getRequest(urlInput);
        System.out.println("Get request output: " + outputJson);
        
        // ASSERT
        List<DateTime> outputWeatherDates = MetaWeatherApiInterface.parseWeatherDates(outputJson);

        int index = 0;
        for(index = 0; index < outputWeatherDates.size(); index++)
        {
            assertEquals("Day did not match", outputWeatherDates.get(index).getDayOfMonth(), dayInput);
            assertEquals("Month did not match", outputWeatherDates.get(index).getMonthOfYear(), monthInput);
            assertEquals("Year did not match", outputWeatherDates.get(index).getYear(), yearInput);            
        }
    }
}