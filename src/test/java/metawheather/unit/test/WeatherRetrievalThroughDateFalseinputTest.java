/**
 * @file WeatherRetrievalThroughDateFalseinputTest.java
 * @author Angelo Sison<angelo.gitb@gmail.com>
 * @date 26/7/2020
 * @brief File contains unit test handling of false dates.
 *
 */

package metaweather.unit.test;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.joda.time.*;



@RunWith(Parameterized.class)
public class WeatherRetrievalThroughDateFalseinputTest {

    private static final int nottinghamWoeId = 30720;
    private int dayInput;
    private int monthInput;
    private int yearInput;

    @Parameterized.Parameters
    public static Collection<Object[]> data() 
    {
        /* {day, month, year, expected result} */
        return Arrays.asList(new Object[][] {
            { 0, 0, 0}, 
            { -1, -1, -1}, 
            { 50, 12, 2015}, 
            { 25, 13, 2015}, 
            { 25, 12, 0}, 
            { 25, -1, 2020}
        });
    }

    public WeatherRetrievalThroughDateFalseinputTest(int day, int month, int year)
    {
        this.dayInput = day;
        this.monthInput = month;
        this.yearInput = year;
    }

    @Before
    public void setUp() {
       System.out.println("\n-----------------------------------------------------------");
    }
    
    /**
     * @brief Erroneous date test
     * This is tested on Nottingham.
     * 
     * - Input condition: day, month, and year integer values
     * - Expected output: Invalid JSON message
     */
    @Test
    public void Weather_InputCorrectDate_ReturnsCorrectState() 
    {
        // ARRANGE
        String urlInput = MetaWeatherApiInterface.ROOT_ENDPOINT 
            + "/api/location" 
            + "/" + nottinghamWoeId 
            + "/" + yearInput
            + "/" + monthInput
            + "/" + dayInput
            + "/";

        System.out.println("URL input: " + urlInput);
        
        // ACT
        String outputJson = MetaWeatherApiInterface.getRequest(urlInput);
        
        // ASSERT
        List<DateTime> outputWeatherDates = MetaWeatherApiInterface.parseWeatherDates(outputJson);
        assertNull("The test date \"" 
            + dayInput
            + "/" + monthInput
            + "/" + yearInput
            + "\" is invalid and shouldn't return the weather info.", 
            outputWeatherDates);
    }
}