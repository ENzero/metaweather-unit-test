/**
 * @file LocationSearchTest.java
 * @author Angelo Sison<angelo.gitb@gmail.com>
 * @date 26/7/2020
 * @brief File contains unit test to test WOE ID through
 * location search.
 *
 */

package metaweather.unit.test;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Collection;

@RunWith(Parameterized.class)
public class LocationSearchTest {
    private final static String woeIdListJsonFileName = "./woeIdList.json";
    private JSONObject locationTestCase;
    private final String woeIdKeyName = "woeid";
    private final String locationKeyName = "name";

    @Parameterized.Parameters
    public static Collection<Object[]> data() {        
        Collection<Object[]> testCases = MetaWeatherApiInterface.parseWoeIdList(woeIdListJsonFileName);
        
        if(testCases == null)
        {
            System.out.println("Was not able to parse file: " + woeIdListJsonFileName);
            return null;
        }
        return testCases;
    }

    public LocationSearchTest(JSONObject testCase)
    {
        this.locationTestCase = testCase;
    }

    @Before
    public void setUp() {
        System.out.println("\n-----------------------------------------------------------");
    }
    
    /**
     * @brief Test Location query
     * 
     * - Input condition: Correct location string
     * - Expected output: Json data containing the correct WOE ID value.
     */
    @Test
    public void Location_InputCorrectLocation_woeIdIsCorrect() 
    {
        // ARRANGE
        String locationNameInput = locationTestCase.getString(locationKeyName);
        int expectedWoeIdValue = locationTestCase.getInt(woeIdKeyName);

        String urlInput = MetaWeatherApiInterface.ROOT_ENDPOINT + "/api/location/search/?query=" + locationNameInput;
        System.out.println("URL input: " + urlInput + "\n");
        System.out.println("expected WOE ID: " + expectedWoeIdValue);
        
        // ACT
        String outputJson = MetaWeatherApiInterface.getRequest(urlInput);
        System.out.println("Get request output: " + outputJson);
        
        // ASSERT
        if(outputJson != null)
        {
            MetaWeatherApiInterface.LocationData locationOutput = MetaWeatherApiInterface.parseLocationMessage(outputJson);
            if(locationOutput != null)
            {
                assertEquals("WOE ID for " + locationNameInput +  " did not match", 
                    locationOutput.woeId, expectedWoeIdValue);
            }
            else
            {
                fail("Metaweather was not able to find location data for " + locationNameInput);
            }
            
        }
        else
        {
            fail("Metaweather was not able to find location data for " + locationNameInput);
        }
    }
}