
/**
 * @file MetaWeatherApiInterface.java
 * @author Angelo Sison<angelo.gitb@gmail.com>
 * @date 26/7/2020
 * @brief File contains MetaWeather API interface and
 * helper functions to set the unit tests
 *
 */

package metaweather.unit.test;

import static org.junit.Assume.assumeNoException;

import java.io.*;
import java.net.*;
import org.json.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.joda.time.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class MetaWeatherApiInterface
{
    public static final String ROOT_ENDPOINT = "https://www.metaweather.com";

    public static class LocationData
    {
        String title;
        String locationType;
        int woeId;
        String lattLong;
    }

    public static class WeatherData
    {
        String title;
        int woeId;
        JSONArray consolidatedWeather;
        JSONArray sources;
        DateTime time;
        String lattLong;
    }

    /**
     * @brief Does a get request and gets the response string
     * @param urlInput: url String
     * @return JSON string if it succeeded and null if it failed
     */
    public static String getRequest(String urlInput)
    {
        URL url = null;
        String returnString = "";
        try
        {
            url = new URL(urlInput);
        }
        catch(MalformedURLException ex)
        {
            ex.printStackTrace();
            return null;
        }

        try
        {
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(new InputStreamReader( con.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) 
            {
                returnString += inputLine;
            }

            in.close();
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
            return null;
        }

        return returnString;
    }

    /**
     * @brief Parses the message that contains the location data JSON
     * Array.
     * @param jsonString: Location data JSON string
     * @return LocationData data object that contains the key values or null
     */
    public static LocationData parseLocationMessage(String jsonString)
    {
        JSONArray jsonarray;
        
        if(jsonString == null)
        {
            System.out.println("JSON String is empty.");
            return null;
        }

        try
        {
            jsonarray = new JSONArray(jsonString);
        }
        catch(JSONException e)
        {
            System.out.println("Failed to parse Json stucture");
            System.out.println("JSON: " + jsonString);
            return null;
        }

        if(jsonarray.length() != 1)
        {
            System.out.println("Json Array incorrect. size: " + jsonarray.length());
            return null;
        }

        JSONObject jsonobject = jsonarray.getJSONObject(0);
        LocationData parsedMessage = new LocationData();

        parsedMessage.title = jsonobject.getString("title");
        parsedMessage.locationType = jsonobject.getString("location_type");
        parsedMessage.woeId = jsonobject.getInt("woeid");
        parsedMessage.lattLong = jsonobject.getString("latt_long");

        return parsedMessage;
    }

    /**
     * @brief Parses the message that contains the location data JSON
     * Array.
     * @param jsonString: Weather data JSON string
     * @return WeatherData data object that contains the key values or null
     */
    public static WeatherData  parseWeatherMessage(String jsonString)
    {
        if(jsonString == null)
        {
            System.out.println("JSON String is empty.");
            return null;
        }

        JSONObject obj;
        
        try {
            obj = new JSONObject(jsonString);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        
        WeatherData parsedMessage = new WeatherData();
        parsedMessage.title = obj.getString("title");
        parsedMessage.woeId = obj.getInt("woeid");
        parsedMessage.consolidatedWeather = obj.getJSONArray("consolidated_weather");
        parsedMessage.sources = obj.getJSONArray("sources");
        parsedMessage.lattLong = obj.getString("latt_long");
        parsedMessage.time = DateTime.parse(obj.getString("time"));
        return parsedMessage;
    }

    /**
     * @brief Parses the dates of weather forcast nodes.
     * @param jsonString: Weather data JSON array string
     * @return list of wheather forecast dates or null
     */
    public static List<DateTime> parseWeatherDates(String jsonString)
    {
        JSONArray jsonarray;

        if(jsonString == null)
        {
            System.out.println("JSON String is empty.");
            return null;
        }

        try
        {
            jsonarray = new JSONArray(jsonString);
        }
        catch(JSONException e)
        {
            System.out.println("Failed to parse Json stucture");
            System.out.println("JSON: " + jsonString);
            return null;
        }
        
        System.out.println("Weather size: " + jsonarray.length());

        List<DateTime> weatherDateList = new ArrayList<>();

        int index =0;
        for(index = 0; index < jsonarray.length(); index++)
        {
            JSONObject currObject = jsonarray.getJSONObject(index);
        
            String date = currObject.getString("applicable_date");
            weatherDateList.add(DateTime.parse(date));
        }

        return weatherDateList;
    }

    /**
     * @brief Parses a json file that contains a list of all
     * WOE ID entries. It returns town type location etries.
     * @param fileName: WOE ID json file location
     * @return list of town locations
     */
    @SuppressWarnings("unchecked")
    public static Collection<Object[]> parseWoeIdList(String fileName)
    {
        JSONParser jsonParser = new JSONParser();
        Collection<Object[]> locationList = new ArrayList<Object[]>();
        try
        {
            FileReader reader = new FileReader(fileName);

            //Read JSON file
            Object obj = jsonParser.parse(reader);
            JSONArray woeIdList = new JSONArray(obj.toString());

            
            System.out.println("Location test list (" + woeIdList.length() + "):");
            
            /* Only pick towns */
            int index = 0;
            for(index = 0; index < woeIdList.length(); index++)
            {
                JSONObject placeTypeObject = woeIdList.getJSONObject(index).getJSONObject("placeType");
                if(placeTypeObject.getString("name").equals("Town"))
                {
                    System.out.println("[" + index + "]:" + woeIdList.getJSONObject(index).getString("name")
                        + " -> " + woeIdList.getJSONObject(index).getInt("woeid"));

                    locationList.add(new Object[]{woeIdList.getJSONObject(index)});
               }
            }
        } 
        catch (FileNotFoundException e) 
        {
            e.printStackTrace();
            return null;
        } 
        catch (IOException e) {
            e.printStackTrace();
            return null;
        } 
        catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
        catch(JSONException e)
        {
            e.printStackTrace();
            return null;
        }

        return locationList;

    }
}
